"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var nativescript_module_1 = require("nativescript-angular/nativescript.module");
var app_component_1 = require("./app.component");
var board_component_1 = require("./views/board/board.component");
var bomb_component_1 = require("./bomb/bomb.component");
var square_state_image_pipe_1 = require("./square-state-image.pipe");
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            declarations: [
                app_component_1.AppComponent,
                board_component_1.BoardComponent,
                bomb_component_1.BombComponent,
                square_state_image_pipe_1.SquareStateImagePipe
            ],
            bootstrap: [app_component_1.AppComponent],
            imports: [nativescript_module_1.NativeScriptModule],
            schemas: [core_1.NO_ERRORS_SCHEMA]
        })
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImFwcC5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBMEQ7QUFDMUQsZ0ZBQTZFO0FBRTdFLGlEQUE4QztBQUM5QyxpRUFBOEQ7QUFDOUQsd0RBQXFEO0FBRXJELHFFQUFnRTtBQWFoRTtJQUFBO0lBQXdCLENBQUM7SUFBWixTQUFTO1FBWHJCLGVBQVEsQ0FBQztZQUNSLFlBQVksRUFBRTtnQkFDWiw0QkFBWTtnQkFDWixnQ0FBYztnQkFDZCw4QkFBYTtnQkFDYiw4Q0FBb0I7YUFDckI7WUFDRCxTQUFTLEVBQUUsQ0FBQyw0QkFBWSxDQUFDO1lBQ3pCLE9BQU8sRUFBRSxDQUFDLHdDQUFrQixDQUFDO1lBQzdCLE9BQU8sRUFBRSxDQUFDLHVCQUFnQixDQUFDO1NBQzVCLENBQUM7T0FDVyxTQUFTLENBQUc7SUFBRCxnQkFBQztDQUFBLEFBQXpCLElBQXlCO0FBQVosOEJBQVMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSwgTk9fRVJST1JTX1NDSEVNQSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnXG5pbXBvcnQgeyBOYXRpdmVTY3JpcHRNb2R1bGUgfSBmcm9tICduYXRpdmVzY3JpcHQtYW5ndWxhci9uYXRpdmVzY3JpcHQubW9kdWxlJ1xuXG5pbXBvcnQgeyBBcHBDb21wb25lbnQgfSBmcm9tICcuL2FwcC5jb21wb25lbnQnXG5pbXBvcnQgeyBCb2FyZENvbXBvbmVudCB9IGZyb20gJy4vdmlld3MvYm9hcmQvYm9hcmQuY29tcG9uZW50J1xuaW1wb3J0IHsgQm9tYkNvbXBvbmVudCB9IGZyb20gJy4vYm9tYi9ib21iLmNvbXBvbmVudCdcblxuaW1wb3J0IHsgU3F1YXJlU3RhdGVJbWFnZVBpcGUgfSBmcm9tICcuL3NxdWFyZS1zdGF0ZS1pbWFnZS5waXBlJ1xuXG5ATmdNb2R1bGUoe1xuICBkZWNsYXJhdGlvbnM6IFtcbiAgICBBcHBDb21wb25lbnQsXG4gICAgQm9hcmRDb21wb25lbnQsXG4gICAgQm9tYkNvbXBvbmVudCxcbiAgICBTcXVhcmVTdGF0ZUltYWdlUGlwZVxuICBdLFxuICBib290c3RyYXA6IFtBcHBDb21wb25lbnRdLFxuICBpbXBvcnRzOiBbTmF0aXZlU2NyaXB0TW9kdWxlXSxcbiAgc2NoZW1hczogW05PX0VSUk9SU19TQ0hFTUFdXG59KVxuZXhwb3J0IGNsYXNzIEFwcE1vZHVsZSB7fVxuIl19