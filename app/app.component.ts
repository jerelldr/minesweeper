import { Component } from '@angular/core'
import { Fontawesome } from 'nativescript-fontawesome'
Fontawesome.init()

@Component({
  moduleId: module.id,
  selector: 'my-app',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  // Your TypeScript logic goes here
}
