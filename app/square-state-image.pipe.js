"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var domain_1 = require("./domain");
var SquareStateImagePipe = /** @class */ (function () {
    function SquareStateImagePipe() {
    }
    SquareStateImagePipe.prototype.transform = function (value) {
        switch (value) {
            case domain_1.SquareState.Covered:
                return '';
            case domain_1.SquareState.Uncovered:
                return '~/images/circle.png';
            case domain_1.SquareState.Flagged:
                return '~/images/cross.png';
        }
    };
    SquareStateImagePipe = __decorate([
        core_1.Pipe({ name: 'squareStateImage' })
    ], SquareStateImagePipe);
    return SquareStateImagePipe;
}());
exports.SquareStateImagePipe = SquareStateImagePipe;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3F1YXJlLXN0YXRlLWltYWdlLnBpcGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzcXVhcmUtc3RhdGUtaW1hZ2UucGlwZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUFtRDtBQUNuRCxtQ0FBOEM7QUFHOUM7SUFBQTtJQVdBLENBQUM7SUFWQyx3Q0FBUyxHQUFULFVBQVUsS0FBa0I7UUFDMUIsTUFBTSxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztZQUNkLEtBQUssb0JBQVcsQ0FBQyxPQUFPO2dCQUN0QixNQUFNLENBQUMsRUFBRSxDQUFBO1lBQ1gsS0FBSyxvQkFBVyxDQUFDLFNBQVM7Z0JBQ3hCLE1BQU0sQ0FBQyxxQkFBcUIsQ0FBQTtZQUM5QixLQUFLLG9CQUFXLENBQUMsT0FBTztnQkFDdEIsTUFBTSxDQUFDLG9CQUFvQixDQUFBO1FBQy9CLENBQUM7SUFDSCxDQUFDO0lBVlUsb0JBQW9CO1FBRGhDLFdBQUksQ0FBQyxFQUFFLElBQUksRUFBRSxrQkFBa0IsRUFBRSxDQUFDO09BQ3RCLG9CQUFvQixDQVdoQztJQUFELDJCQUFDO0NBQUEsQUFYRCxJQVdDO0FBWFksb0RBQW9CIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgUGlwZSwgUGlwZVRyYW5zZm9ybSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnXG5pbXBvcnQgeyBTcXVhcmUsIFNxdWFyZVN0YXRlIH0gZnJvbSAnLi9kb21haW4nXG5cbkBQaXBlKHsgbmFtZTogJ3NxdWFyZVN0YXRlSW1hZ2UnIH0pXG5leHBvcnQgY2xhc3MgU3F1YXJlU3RhdGVJbWFnZVBpcGUgaW1wbGVtZW50cyBQaXBlVHJhbnNmb3JtIHtcbiAgdHJhbnNmb3JtKHZhbHVlOiBTcXVhcmVTdGF0ZSk6IHN0cmluZyB7XG4gICAgc3dpdGNoICh2YWx1ZSkge1xuICAgICAgY2FzZSBTcXVhcmVTdGF0ZS5Db3ZlcmVkOlxuICAgICAgICByZXR1cm4gJydcbiAgICAgIGNhc2UgU3F1YXJlU3RhdGUuVW5jb3ZlcmVkOlxuICAgICAgICByZXR1cm4gJ34vaW1hZ2VzL2NpcmNsZS5wbmcnXG4gICAgICBjYXNlIFNxdWFyZVN0YXRlLkZsYWdnZWQ6XG4gICAgICAgIHJldHVybiAnfi9pbWFnZXMvY3Jvc3MucG5nJ1xuICAgIH1cbiAgfVxufVxuIl19