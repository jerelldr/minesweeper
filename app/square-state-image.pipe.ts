import { Pipe, PipeTransform } from '@angular/core'
import { Square, SquareState } from './domain'

@Pipe({ name: 'squareStateImage' })
export class SquareStateImagePipe implements PipeTransform {
  transform(value: SquareState): string {
    switch (value) {
      case SquareState.Covered:
        return ''
      case SquareState.Uncovered:
        return '~/images/circle.png'
      case SquareState.Flagged:
        return '~/images/cross.png'
    }
  }
}
