import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core'
import { NativeScriptModule } from 'nativescript-angular/nativescript.module'

import { AppComponent } from './app.component'
import { BoardComponent } from './views/board/board.component'
import { BombComponent } from './bomb/bomb.component'

import { SquareStateImagePipe } from './square-state-image.pipe'

@NgModule({
  declarations: [
    AppComponent,
    BoardComponent,
    BombComponent,
    SquareStateImagePipe
  ],
  bootstrap: [AppComponent],
  imports: [NativeScriptModule],
  schemas: [NO_ERRORS_SCHEMA]
})
export class AppModule {}
