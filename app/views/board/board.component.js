"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var _1 = require("../../domain/");
var dialogs = require("ui/dialogs");
var BoardComponent = /** @class */ (function () {
    function BoardComponent() {
        this.board = new _1.Board(8);
    }
    BoardComponent.prototype.startNewGame = function () {
        var title = "Restart";
        var message = 'Restart Game?';
        var type = 'restart';
        this.showDialog(title, message, type);
    };
    BoardComponent.prototype.mark = function (square) {
        if (this.canChangeState(square) === false) {
            return;
        }
        this.board.mark(square);
        this.checkScore();
    };
    BoardComponent.prototype.reveal = function (square) {
        if (this.canChangeState(square) === false) {
            return;
        }
        if (this.board.isMine(square.xCoord, square.yCoord)) {
            var title = 'Game Over';
            var message = "Bomb Clicked at row:" + square.xCoord + " col:" + square.yCoord;
            this.showDialog(title, message, null);
        }
        else {
            this.board.reveal(square);
            this.checkScore();
        }
    };
    BoardComponent.prototype.checkScore = function () {
        if (this.board.gameOver() === this.board.length) {
            var title = "Game Won!!";
            var message = 'Restart Game?';
            this.showDialog(title, message, null);
        }
        else {
            // console.dir({
            //   score: this.board.score,
            //   uncovered: JSON.stringify(this.board.uncovered)
            // })
        }
    };
    BoardComponent.prototype.showDialog = function (title, message, type) {
        var _this = this;
        if (type === 'restart') {
            this.restart(title, message);
        }
        else {
            dialogs
                .alert({
                title: title,
                message: message,
                okButtonText: 'Restart'
            })
                .then(function (r) {
                _this.board.startNewGame();
            });
        }
    };
    BoardComponent.prototype.canChangeState = function (square) {
        return square.canChangeState;
    };
    BoardComponent.prototype.restart = function (title, message) {
        var _this = this;
        dialogs
            .prompt({
            title: title,
            message: message,
            defaultText: this.board.size.toString(),
            okButtonText: 'Restart'
        })
            .then(function (r) {
            _this.board.setBoardSize(parseInt(r.text));
        });
    };
    Object.defineProperty(BoardComponent.prototype, "boardSideSpecification", {
        get: function () {
            var specs = [];
            for (var i = 0; i < this.board.size; i++) {
                specs.push('*');
            }
            return specs.join(',');
        },
        enumerable: true,
        configurable: true
    });
    BoardComponent.prototype.classOf = function (square) {
        return square.state === _1.SquareState.Uncovered
            ? "light-square"
            : 'dark-square';
    };
    BoardComponent.prototype.numberColor = function (square) {
        switch (square.adjSquareCount) {
            case 1:
                return 'light-blue';
            case 2:
                return 'green';
            case 3:
                return 'red';
            case 4:
                return 'dark-blue';
            case 5:
                return 'brown';
            case 6:
                return 'aqua';
            case 7:
                return 'black';
            case 8:
                return 'light-grey';
            default:
                return 'black';
        }
    };
    BoardComponent.prototype.ngOnInit = function () { };
    BoardComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'board',
            templateUrl: 'board.component.html',
            styleUrls: ['board.component.scss']
        })
    ], BoardComponent);
    return BoardComponent;
}());
exports.BoardComponent = BoardComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYm9hcmQuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiYm9hcmQuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQWlEO0FBQ2pELGtDQUEwRDtBQUMxRCxvQ0FBcUM7QUFVckM7SUFOQTtRQU9TLFVBQUssR0FBVSxJQUFJLFFBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQTtJQXFIcEMsQ0FBQztJQW5IUSxxQ0FBWSxHQUFuQjtRQUNFLElBQUksS0FBSyxHQUFHLFNBQVMsQ0FBQTtRQUNyQixJQUFJLE9BQU8sR0FBRyxlQUFlLENBQUE7UUFDN0IsSUFBSSxJQUFJLEdBQUcsU0FBUyxDQUFBO1FBQ3BCLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxFQUFFLE9BQU8sRUFBRSxJQUFJLENBQUMsQ0FBQTtJQUN2QyxDQUFDO0lBRU0sNkJBQUksR0FBWCxVQUFZLE1BQU07UUFDaEIsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsS0FBSyxLQUFLLENBQUMsQ0FBQyxDQUFDO1lBQzFDLE1BQU0sQ0FBQTtRQUNSLENBQUM7UUFFRCxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQTtRQUN2QixJQUFJLENBQUMsVUFBVSxFQUFFLENBQUE7SUFDbkIsQ0FBQztJQUVNLCtCQUFNLEdBQWIsVUFBYyxNQUFNO1FBQ2xCLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsTUFBTSxDQUFDLEtBQUssS0FBSyxDQUFDLENBQUMsQ0FBQztZQUMxQyxNQUFNLENBQUE7UUFDUixDQUFDO1FBRUQsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLE1BQU0sRUFBRSxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ3BELElBQUksS0FBSyxHQUFHLFdBQVcsQ0FBQTtZQUN2QixJQUFJLE9BQU8sR0FBRyx5QkFBdUIsTUFBTSxDQUFDLE1BQU0sYUFBUSxNQUFNLENBQUMsTUFBUSxDQUFBO1lBQ3pFLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxFQUFFLE9BQU8sRUFBRSxJQUFJLENBQUMsQ0FBQTtRQUN2QyxDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDTixJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQTtZQUN6QixJQUFJLENBQUMsVUFBVSxFQUFFLENBQUE7UUFDbkIsQ0FBQztJQUNILENBQUM7SUFFTSxtQ0FBVSxHQUFqQjtRQUNFLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxFQUFFLEtBQUssSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO1lBQ2hELElBQUksS0FBSyxHQUFHLFlBQVksQ0FBQTtZQUN4QixJQUFJLE9BQU8sR0FBRyxlQUFlLENBQUE7WUFDN0IsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLEVBQUUsT0FBTyxFQUFFLElBQUksQ0FBQyxDQUFBO1FBQ3ZDLENBQUM7UUFBQyxJQUFJLENBQUMsQ0FBQztZQUNOLGdCQUFnQjtZQUNoQiw2QkFBNkI7WUFDN0Isb0RBQW9EO1lBQ3BELEtBQUs7UUFDUCxDQUFDO0lBQ0gsQ0FBQztJQUVNLG1DQUFVLEdBQWpCLFVBQWtCLEtBQWEsRUFBRSxPQUFlLEVBQUUsSUFBWTtRQUE5RCxpQkFjQztRQWJDLEVBQUUsQ0FBQyxDQUFDLElBQUksS0FBSyxTQUFTLENBQUMsQ0FBQyxDQUFDO1lBQ3ZCLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxFQUFFLE9BQU8sQ0FBQyxDQUFBO1FBQzlCLENBQUM7UUFBQyxJQUFJLENBQUMsQ0FBQztZQUNOLE9BQU87aUJBQ0osS0FBSyxDQUFDO2dCQUNMLEtBQUssRUFBRSxLQUFLO2dCQUNaLE9BQU8sRUFBRSxPQUFPO2dCQUNoQixZQUFZLEVBQUUsU0FBUzthQUN4QixDQUFDO2lCQUNELElBQUksQ0FBQyxVQUFBLENBQUM7Z0JBQ0wsS0FBSSxDQUFDLEtBQUssQ0FBQyxZQUFZLEVBQUUsQ0FBQTtZQUMzQixDQUFDLENBQUMsQ0FBQTtRQUNOLENBQUM7SUFDSCxDQUFDO0lBRU8sdUNBQWMsR0FBdEIsVUFBdUIsTUFBTTtRQUMzQixNQUFNLENBQUMsTUFBTSxDQUFDLGNBQWMsQ0FBQTtJQUM5QixDQUFDO0lBRU8sZ0NBQU8sR0FBZixVQUFnQixLQUFhLEVBQUUsT0FBZTtRQUE5QyxpQkFXQztRQVZDLE9BQU87YUFDSixNQUFNLENBQUM7WUFDTixLQUFLLEVBQUUsS0FBSztZQUNaLE9BQU8sRUFBRSxPQUFPO1lBQ2hCLFdBQVcsRUFBRSxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUU7WUFDdkMsWUFBWSxFQUFFLFNBQVM7U0FDeEIsQ0FBQzthQUNELElBQUksQ0FBQyxVQUFBLENBQUM7WUFDTCxLQUFJLENBQUMsS0FBSyxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUE7UUFDM0MsQ0FBQyxDQUFDLENBQUE7SUFDTixDQUFDO0lBRUQsc0JBQVcsa0RBQXNCO2FBQWpDO1lBQ0UsSUFBSSxLQUFLLEdBQUcsRUFBRSxDQUFBO1lBQ2QsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDO2dCQUN6QyxLQUFLLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFBO1lBQ2pCLENBQUM7WUFDRCxNQUFNLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQTtRQUN4QixDQUFDOzs7T0FBQTtJQUVNLGdDQUFPLEdBQWQsVUFBZSxNQUFjO1FBQzNCLE1BQU0sQ0FBQyxNQUFNLENBQUMsS0FBSyxLQUFLLGNBQVcsQ0FBQyxTQUFTO1lBQzNDLENBQUMsQ0FBQyxjQUFjO1lBQ2hCLENBQUMsQ0FBQyxhQUFhLENBQUE7SUFDbkIsQ0FBQztJQUVNLG9DQUFXLEdBQWxCLFVBQW1CLE1BQWM7UUFDL0IsTUFBTSxDQUFDLENBQUMsTUFBTSxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUM7WUFDOUIsS0FBSyxDQUFDO2dCQUNKLE1BQU0sQ0FBQyxZQUFZLENBQUE7WUFDckIsS0FBSyxDQUFDO2dCQUNKLE1BQU0sQ0FBQyxPQUFPLENBQUE7WUFDaEIsS0FBSyxDQUFDO2dCQUNKLE1BQU0sQ0FBQyxLQUFLLENBQUE7WUFDZCxLQUFLLENBQUM7Z0JBQ0osTUFBTSxDQUFDLFdBQVcsQ0FBQTtZQUNwQixLQUFLLENBQUM7Z0JBQ0osTUFBTSxDQUFDLE9BQU8sQ0FBQTtZQUNoQixLQUFLLENBQUM7Z0JBQ0osTUFBTSxDQUFDLE1BQU0sQ0FBQTtZQUNmLEtBQUssQ0FBQztnQkFDSixNQUFNLENBQUMsT0FBTyxDQUFBO1lBQ2hCLEtBQUssQ0FBQztnQkFDSixNQUFNLENBQUMsWUFBWSxDQUFBO1lBQ3JCO2dCQUNFLE1BQU0sQ0FBQyxPQUFPLENBQUE7UUFDbEIsQ0FBQztJQUNILENBQUM7SUFFRCxpQ0FBUSxHQUFSLGNBQVksQ0FBQztJQXJIRixjQUFjO1FBTjFCLGdCQUFTLENBQUM7WUFDVCxRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7WUFDbkIsUUFBUSxFQUFFLE9BQU87WUFDakIsV0FBVyxFQUFFLHNCQUFzQjtZQUNuQyxTQUFTLEVBQUUsQ0FBQyxzQkFBc0IsQ0FBQztTQUNwQyxDQUFDO09BQ1csY0FBYyxDQXNIMUI7SUFBRCxxQkFBQztDQUFBLEFBdEhELElBc0hDO0FBdEhZLHdDQUFjIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJ1xuaW1wb3J0IHsgQm9hcmQsIFNxdWFyZSwgU3F1YXJlU3RhdGUgfSBmcm9tICcuLi8uLi9kb21haW4vJ1xuaW1wb3J0ICogYXMgZGlhbG9ncyBmcm9tICd1aS9kaWFsb2dzJ1xuXG5pbXBvcnQgKiBhcyBwbGF0Zm9ybSBmcm9tICdwbGF0Zm9ybSdcblxuQENvbXBvbmVudCh7XG4gIG1vZHVsZUlkOiBtb2R1bGUuaWQsXG4gIHNlbGVjdG9yOiAnYm9hcmQnLFxuICB0ZW1wbGF0ZVVybDogJ2JvYXJkLmNvbXBvbmVudC5odG1sJyxcbiAgc3R5bGVVcmxzOiBbJ2JvYXJkLmNvbXBvbmVudC5zY3NzJ11cbn0pXG5leHBvcnQgY2xhc3MgQm9hcmRDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuICBwdWJsaWMgYm9hcmQ6IEJvYXJkID0gbmV3IEJvYXJkKDgpXG5cbiAgcHVibGljIHN0YXJ0TmV3R2FtZSgpOiB2b2lkIHtcbiAgICBsZXQgdGl0bGUgPSBgUmVzdGFydGBcbiAgICBsZXQgbWVzc2FnZSA9ICdSZXN0YXJ0IEdhbWU/J1xuICAgIGxldCB0eXBlID0gJ3Jlc3RhcnQnXG4gICAgdGhpcy5zaG93RGlhbG9nKHRpdGxlLCBtZXNzYWdlLCB0eXBlKVxuICB9XG5cbiAgcHVibGljIG1hcmsoc3F1YXJlKTogdm9pZCB7XG4gICAgaWYgKHRoaXMuY2FuQ2hhbmdlU3RhdGUoc3F1YXJlKSA9PT0gZmFsc2UpIHtcbiAgICAgIHJldHVyblxuICAgIH1cblxuICAgIHRoaXMuYm9hcmQubWFyayhzcXVhcmUpXG4gICAgdGhpcy5jaGVja1Njb3JlKClcbiAgfVxuXG4gIHB1YmxpYyByZXZlYWwoc3F1YXJlKTogdm9pZCB7XG4gICAgaWYgKHRoaXMuY2FuQ2hhbmdlU3RhdGUoc3F1YXJlKSA9PT0gZmFsc2UpIHtcbiAgICAgIHJldHVyblxuICAgIH1cblxuICAgIGlmICh0aGlzLmJvYXJkLmlzTWluZShzcXVhcmUueENvb3JkLCBzcXVhcmUueUNvb3JkKSkge1xuICAgICAgbGV0IHRpdGxlID0gJ0dhbWUgT3ZlcidcbiAgICAgIGxldCBtZXNzYWdlID0gYEJvbWIgQ2xpY2tlZCBhdCByb3c6JHtzcXVhcmUueENvb3JkfSBjb2w6JHtzcXVhcmUueUNvb3JkfWBcbiAgICAgIHRoaXMuc2hvd0RpYWxvZyh0aXRsZSwgbWVzc2FnZSwgbnVsbClcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5ib2FyZC5yZXZlYWwoc3F1YXJlKVxuICAgICAgdGhpcy5jaGVja1Njb3JlKClcbiAgICB9XG4gIH1cblxuICBwdWJsaWMgY2hlY2tTY29yZSgpIHtcbiAgICBpZiAodGhpcy5ib2FyZC5nYW1lT3ZlcigpID09PSB0aGlzLmJvYXJkLmxlbmd0aCkge1xuICAgICAgbGV0IHRpdGxlID0gYEdhbWUgV29uISFgXG4gICAgICBsZXQgbWVzc2FnZSA9ICdSZXN0YXJ0IEdhbWU/J1xuICAgICAgdGhpcy5zaG93RGlhbG9nKHRpdGxlLCBtZXNzYWdlLCBudWxsKVxuICAgIH0gZWxzZSB7XG4gICAgICAvLyBjb25zb2xlLmRpcih7XG4gICAgICAvLyAgIHNjb3JlOiB0aGlzLmJvYXJkLnNjb3JlLFxuICAgICAgLy8gICB1bmNvdmVyZWQ6IEpTT04uc3RyaW5naWZ5KHRoaXMuYm9hcmQudW5jb3ZlcmVkKVxuICAgICAgLy8gfSlcbiAgICB9XG4gIH1cblxuICBwdWJsaWMgc2hvd0RpYWxvZyh0aXRsZTogc3RyaW5nLCBtZXNzYWdlOiBzdHJpbmcsIHR5cGU6IHN0cmluZyk6IHZvaWQge1xuICAgIGlmICh0eXBlID09PSAncmVzdGFydCcpIHtcbiAgICAgIHRoaXMucmVzdGFydCh0aXRsZSwgbWVzc2FnZSlcbiAgICB9IGVsc2Uge1xuICAgICAgZGlhbG9nc1xuICAgICAgICAuYWxlcnQoe1xuICAgICAgICAgIHRpdGxlOiB0aXRsZSxcbiAgICAgICAgICBtZXNzYWdlOiBtZXNzYWdlLFxuICAgICAgICAgIG9rQnV0dG9uVGV4dDogJ1Jlc3RhcnQnXG4gICAgICAgIH0pXG4gICAgICAgIC50aGVuKHIgPT4ge1xuICAgICAgICAgIHRoaXMuYm9hcmQuc3RhcnROZXdHYW1lKClcbiAgICAgICAgfSlcbiAgICB9XG4gIH1cblxuICBwcml2YXRlIGNhbkNoYW5nZVN0YXRlKHNxdWFyZSkge1xuICAgIHJldHVybiBzcXVhcmUuY2FuQ2hhbmdlU3RhdGVcbiAgfVxuXG4gIHByaXZhdGUgcmVzdGFydCh0aXRsZTogc3RyaW5nLCBtZXNzYWdlOiBzdHJpbmcpIHtcbiAgICBkaWFsb2dzXG4gICAgICAucHJvbXB0KHtcbiAgICAgICAgdGl0bGU6IHRpdGxlLFxuICAgICAgICBtZXNzYWdlOiBtZXNzYWdlLFxuICAgICAgICBkZWZhdWx0VGV4dDogdGhpcy5ib2FyZC5zaXplLnRvU3RyaW5nKCksXG4gICAgICAgIG9rQnV0dG9uVGV4dDogJ1Jlc3RhcnQnXG4gICAgICB9KVxuICAgICAgLnRoZW4ociA9PiB7XG4gICAgICAgIHRoaXMuYm9hcmQuc2V0Qm9hcmRTaXplKHBhcnNlSW50KHIudGV4dCkpXG4gICAgICB9KVxuICB9XG5cbiAgcHVibGljIGdldCBib2FyZFNpZGVTcGVjaWZpY2F0aW9uKCk6IHN0cmluZyB7XG4gICAgdmFyIHNwZWNzID0gW11cbiAgICBmb3IgKGxldCBpID0gMDsgaSA8IHRoaXMuYm9hcmQuc2l6ZTsgaSsrKSB7XG4gICAgICBzcGVjcy5wdXNoKCcqJylcbiAgICB9XG4gICAgcmV0dXJuIHNwZWNzLmpvaW4oJywnKVxuICB9XG5cbiAgcHVibGljIGNsYXNzT2Yoc3F1YXJlOiBTcXVhcmUpOiBzdHJpbmcge1xuICAgIHJldHVybiBzcXVhcmUuc3RhdGUgPT09IFNxdWFyZVN0YXRlLlVuY292ZXJlZFxuICAgICAgPyBgbGlnaHQtc3F1YXJlYFxuICAgICAgOiAnZGFyay1zcXVhcmUnXG4gIH1cblxuICBwdWJsaWMgbnVtYmVyQ29sb3Ioc3F1YXJlOiBTcXVhcmUpOiBzdHJpbmcge1xuICAgIHN3aXRjaCAoc3F1YXJlLmFkalNxdWFyZUNvdW50KSB7XG4gICAgICBjYXNlIDE6XG4gICAgICAgIHJldHVybiAnbGlnaHQtYmx1ZSdcbiAgICAgIGNhc2UgMjpcbiAgICAgICAgcmV0dXJuICdncmVlbidcbiAgICAgIGNhc2UgMzpcbiAgICAgICAgcmV0dXJuICdyZWQnXG4gICAgICBjYXNlIDQ6XG4gICAgICAgIHJldHVybiAnZGFyay1ibHVlJ1xuICAgICAgY2FzZSA1OlxuICAgICAgICByZXR1cm4gJ2Jyb3duJ1xuICAgICAgY2FzZSA2OlxuICAgICAgICByZXR1cm4gJ2FxdWEnXG4gICAgICBjYXNlIDc6XG4gICAgICAgIHJldHVybiAnYmxhY2snXG4gICAgICBjYXNlIDg6XG4gICAgICAgIHJldHVybiAnbGlnaHQtZ3JleSdcbiAgICAgIGRlZmF1bHQ6XG4gICAgICAgIHJldHVybiAnYmxhY2snXG4gICAgfVxuICB9XG5cbiAgbmdPbkluaXQoKSB7fVxufVxuIl19