import { Component, OnInit } from '@angular/core'
import { Board, Square, SquareState } from '../../domain/'
import * as dialogs from 'ui/dialogs'

import * as platform from 'platform'

@Component({
  moduleId: module.id,
  selector: 'board',
  templateUrl: 'board.component.html',
  styleUrls: ['board.component.scss']
})
export class BoardComponent implements OnInit {
  public board: Board = new Board(8)

  public startNewGame(): void {
    let title = `Restart`
    let message = 'Restart Game?'
    let type = 'restart'
    this.showDialog(title, message, type)
  }

  public mark(square): void {
    if (this.canChangeState(square) === false) {
      return
    }

    this.board.mark(square)
    this.checkScore()
  }

  public reveal(square): void {
    if (this.canChangeState(square) === false) {
      return
    }

    if (this.board.isMine(square.xCoord, square.yCoord)) {
      let title = 'Game Over'
      let message = `Bomb Clicked at row:${square.xCoord} col:${square.yCoord}`
      this.showDialog(title, message, null)
    } else {
      this.board.reveal(square)
      this.checkScore()
    }
  }

  public checkScore() {
    if (this.board.gameOver() === this.board.length) {
      let title = `Game Won!!`
      let message = 'Restart Game?'
      this.showDialog(title, message, null)
    } else {
      // console.dir({
      //   score: this.board.score,
      //   uncovered: JSON.stringify(this.board.uncovered)
      // })
    }
  }

  public showDialog(title: string, message: string, type: string): void {
    if (type === 'restart') {
      this.restart(title, message)
    } else {
      dialogs
        .alert({
          title: title,
          message: message,
          okButtonText: 'Restart'
        })
        .then(r => {
          this.board.startNewGame()
        })
    }
  }

  private canChangeState(square) {
    return square.canChangeState
  }

  private restart(title: string, message: string) {
    dialogs
      .prompt({
        title: title,
        message: message,
        defaultText: this.board.size.toString(),
        okButtonText: 'Restart'
      })
      .then(r => {
        this.board.setBoardSize(parseInt(r.text))
      })
  }

  public get boardSideSpecification(): string {
    var specs = []
    for (let i = 0; i < this.board.size; i++) {
      specs.push('*')
    }
    return specs.join(',')
  }

  public classOf(square: Square): string {
    return square.state === SquareState.Uncovered
      ? `light-square`
      : 'dark-square'
  }

  public numberColor(square: Square): string {
    switch (square.adjSquareCount) {
      case 1:
        return 'light-blue'
      case 2:
        return 'green'
      case 3:
        return 'red'
      case 4:
        return 'dark-blue'
      case 5:
        return 'brown'
      case 6:
        return 'aqua'
      case 7:
        return 'black'
      case 8:
        return 'light-grey'
      default:
        return 'black'
    }
  }

  ngOnInit() {}
}
