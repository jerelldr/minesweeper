import { Square, SquareState } from './'
import * as dialogs from 'ui/dialogs'

export class Board {
  private _size: number
  private _length: number
  private _mines: number
  private _isGameWon: boolean
  private _currentState: SquareState
  private _flagged: number
  private _uncovered: number
  private _marksCount: number
  private _score: number
  private _squares: Array<Square>

  constructor(size: number) {
    this.setBoardSize(size)
  }

  public get marksCount(): number {
    return this._marksCount
  }

  public get length(): number {
    return this._length
  }

  public get score(): number {
    return this._score
  }

  public get size(): number {
    return this._size
  }

  public get squares(): Array<Square> {
    return this._squares
  }

  public get uncovered(): number {
    return this._uncovered
  }

  public get flagged(): number {
    return this._flagged
  }

  public setBoardSize(size: number): void {
    this._size = size
    this._length = size * size
    this.startNewGame()
  }

  public startNewGame(): void {
    this._score = 0
    this._flagged = 0
    this._uncovered = 0
    this._marksCount = 0
    this._mines = Math.ceil(this._length * 0.15)
    this._currentState = SquareState.Covered

    // console.dir({
    //   message: 'starting new game',
    //   boardSize: this._length,
    //   mines: this._mines,
    //   flagged: this._marksCount
    // })

    this.initializeBoard()
  }

  public gameOver(): number {
    this.getPlayerScore()
    return this._score
  }

  public getPlayerScore(): void {
    let uncovered = 0
    let flagged = 0
    for (let x = 0; x < this._size; x++) {
      for (let y = 0; y < this._size; y++) {
        let idx = this.index(x, y)
        if (this._squares[idx].state === SquareState.Uncovered) {
          //console.dir({ uncovered: this._squares[idx] })

          uncovered++
        } else if (this._squares[idx].state === SquareState.Flagged) {
          //console.dir({ flagged: this._squares[idx] })

          flagged++
        }
      }
    }
    this._flagged = flagged
    this._uncovered = uncovered
    this._score = this._flagged + this._uncovered
  }

  public mark(square: Square): void {
    if (square.canChangeState) {
      square.state = SquareState.Flagged
    }
  }

  public unCoverSquare(square: Square): void {
    if (square.canChangeState) {
      square.state = SquareState.Uncovered
    }
  }

  public reveal(square: Square): void {
    let idx = this.index(square.xCoord, square.yCoord)
    let checked = new Set<Square>()
    this.floodFill(square.xCoord, square.yCoord, checked)
  }

  private changeCurrentState(square: Square) {
    square.state = this.nextState(square.state)
  }

  private nextState(state: SquareState): SquareState {
    if (state == SquareState.Covered) {
      return SquareState.Flagged
    } else if (state == SquareState.Flagged) {
      return SquareState.Uncovered
    } else if (state == SquareState.Uncovered) {
      return SquareState.Covered
    }
  }

  private placeRandomMines() {
    let placed = 0
    while (placed < this._mines) {
      let row = this.randomIndex()
      let col = this.randomIndex()
      if (
        this.isValid(row, col) &&
        this.squares[this.index(row, col)].isMine !== true
      ) {
        this.squares[this.index(row, col)].isMine = true
        placed += 1
      }
    }
  }

  private initializeBoard() {
    this._squares = new Array<Square>()

    for (let x = 0; x < this._size; x++) {
      for (let y = 0; y < this._size; y++) {
        let square = new Square(x, y)
        this._squares.push(square)
      }
    }

    this.placeRandomMines()

    for (let x = 0; x < this._size; x++) {
      for (let y = 0; y < this._size; y++) {
        let idx = this.index(x, y)
        let count = this.countAdjacentMines(this._squares[idx])
        this._squares[idx].adjSquareCount = this.countAdjacentMines(
          this._squares[idx]
        )
      }
    }
  }

  public countAdjacentMines(square: Square): number {
    let count = 0
    let row = square.xCoord
    let col = square.yCoord

    /*

        Cell-->Current Cell (row, col)
        N -->  North        (row-1, col)
        S -->  South        (row+1, col)
        E -->  East         (row, col-1)
        W -->  West         (row, col+1)
        N.E--> North-East   (row-1, col-1)
        N.W--> North-West   (row-1, col+1)
        S.E--> South-East   (row+1, col-1)
        S.W--> South-West   (row+1, col+1)
    */

    //----------- 1st Neighbour (North) ------------
    if (
      this.isValid(row - 1, col) === true &&
      this.isMine(row - 1, col) === true
    ) {
      count++
    }

    //----------- 2nd Neighbour (South) ------------
    if (
      this.isValid(row + 1, col) === true &&
      this.isMine(row + 1, col) == true
    ) {
      count++
    }

    //----------- 3rd Neighbour (East) ------------
    if (
      this.isValid(row, col - 1) === true &&
      this.isMine(row, col - 1) == true
    ) {
      count++
    }

    //----------- 4th Neighbour (West) ------------
    if (
      this.isValid(row, col + 1) === true &&
      this.isMine(row, col + 1) == true
    ) {
      count++
    }

    //----------- 5th Neighbour (North-East) ------------
    if (
      this.isValid(row - 1, col - 1) === true &&
      this.isMine(row - 1, col - 1) == true
    ) {
      count++
    }

    //----------- 6th Neighbour (North-West) ------------
    if (
      this.isValid(row - 1, col + 1) === true &&
      this.isMine(row - 1, col + 1) == true
    ) {
      count++
    }

    //----------- 7th Neighbour (South-East) ------------
    if (
      this.isValid(row + 1, col - 1) === true &&
      this.isMine(row + 1, col - 1) == true
    ) {
      count++
    }

    //----------- 8th Neighbour (South-West) ------------
    if (
      this.isValid(row + 1, col + 1) === true &&
      this.isMine(row + 1, col + 1) === true
    ) {
      count++
    }

    return count
  }

  private index(row: number, col: number): number {
    return row * this._size + col
  }

  private randomIndex(): number {
    return Math.floor(Math.random() * this._size)
  }

  public isMine(row: number, col: number) {
    let idx = this.index(row, col)
    return this._squares[idx].isMine === true
  }

  private isValid(row: number, col: number): boolean {
    if (row < 0 || row >= this._size || (col < 0 || col >= this._size)) {
      return false
    } else {
      return true
    }
  }

  public floodFill(row: number, col: number, checked: Set<Square>) {
    let idx = this.index(row, col)

    if (checked.has(this.squares[idx]) === true) {
      return
    }

    checked.add(this._squares[idx])

    if (this.isValid(row, col) === false || this.isMine(row, col) === true) {
      return
    }

    if (this._squares[idx].state !== SquareState.Covered) {
      return
    }

    //console.log(`oncovered mine at x:${row} y:${col}`)
    this.unCoverSquare(this._squares[idx])
    //console.log(this._squares[idx].adjSquareCount)

    if (this._squares[idx].adjSquareCount > 0) {
      return
    }

    //----------- 4th Neighbour (West) ------------
    this.floodFill(row, col + 1, checked)

    //----------- 3rd Neighbour (East) ------------
    this.floodFill(row, col - 1, checked)

    //----------- 1sd Neighbour (North) ------------
    this.floodFill(row - 1, col, checked)

    //----------- 2nd Neighbour (South) ------------
    this.floodFill(row + 1, col, checked)

    //----------- 8th Neighbour (South-West) ------------
    this.floodFill(row + 1, col + 1, checked)

    //----------- 7th Neighbour (South-East) ------------
    this.floodFill(row + 1, col - 1, checked)

    //----------- 5th Neighbour (North-East) ------------
    this.floodFill(row - 1, col - 1, checked)

    //----------- 6th Neighbour (North-West) ------------
    this.floodFill(row - 1, col + 1, checked)

    // return
  }
}
