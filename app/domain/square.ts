export class Square {
  private _state: SquareState
  private _isMine: boolean
  private _xCoord: number
  private _yCoord: number
  private _adjSquareCount: number

  constructor(x: number, y: number) {
    this._state = SquareState.Covered
    this._isMine = false
    this._xCoord = x
    this._yCoord = y
    this._adjSquareCount = 0
  }

  public get isMine(): boolean {
    return this._isMine
  }

  public set isMine(value: boolean) {
    this._isMine = value
  }

  public get adjSquareCount(): number {
    return this._adjSquareCount
  }

  public set adjSquareCount(value: number) {
    this._adjSquareCount = value
  }

  public get state(): SquareState {
    return this._state
  }

  public set state(value: SquareState) {
    this._state = value
  }

  public get xCoord(): number {
    return this._xCoord
  }

  public get yCoord(): number {
    return this._yCoord
  }

  public get canChangeState(): boolean {
    return this._state === SquareState.Covered
  }
}

export enum SquareState {
  Covered,
  Flagged,
  Uncovered
}
