"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Square = /** @class */ (function () {
    function Square(x, y) {
        this._state = SquareState.Covered;
        this._isMine = false;
        this._xCoord = x;
        this._yCoord = y;
        this._adjSquareCount = 0;
    }
    Object.defineProperty(Square.prototype, "isMine", {
        get: function () {
            return this._isMine;
        },
        set: function (value) {
            this._isMine = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Square.prototype, "adjSquareCount", {
        get: function () {
            return this._adjSquareCount;
        },
        set: function (value) {
            this._adjSquareCount = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Square.prototype, "state", {
        get: function () {
            return this._state;
        },
        set: function (value) {
            this._state = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Square.prototype, "xCoord", {
        get: function () {
            return this._xCoord;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Square.prototype, "yCoord", {
        get: function () {
            return this._yCoord;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Square.prototype, "canChangeState", {
        get: function () {
            return this._state === SquareState.Covered;
        },
        enumerable: true,
        configurable: true
    });
    return Square;
}());
exports.Square = Square;
var SquareState;
(function (SquareState) {
    SquareState[SquareState["Covered"] = 0] = "Covered";
    SquareState[SquareState["Flagged"] = 1] = "Flagged";
    SquareState[SquareState["Uncovered"] = 2] = "Uncovered";
})(SquareState = exports.SquareState || (exports.SquareState = {}));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3F1YXJlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsic3F1YXJlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUE7SUFPRSxnQkFBWSxDQUFTLEVBQUUsQ0FBUztRQUM5QixJQUFJLENBQUMsTUFBTSxHQUFHLFdBQVcsQ0FBQyxPQUFPLENBQUE7UUFDakMsSUFBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUE7UUFDcEIsSUFBSSxDQUFDLE9BQU8sR0FBRyxDQUFDLENBQUE7UUFDaEIsSUFBSSxDQUFDLE9BQU8sR0FBRyxDQUFDLENBQUE7UUFDaEIsSUFBSSxDQUFDLGVBQWUsR0FBRyxDQUFDLENBQUE7SUFDMUIsQ0FBQztJQUVELHNCQUFXLDBCQUFNO2FBQWpCO1lBQ0UsTUFBTSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUE7UUFDckIsQ0FBQzthQUVELFVBQWtCLEtBQWM7WUFDOUIsSUFBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUE7UUFDdEIsQ0FBQzs7O09BSkE7SUFNRCxzQkFBVyxrQ0FBYzthQUF6QjtZQUNFLE1BQU0sQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFBO1FBQzdCLENBQUM7YUFFRCxVQUEwQixLQUFhO1lBQ3JDLElBQUksQ0FBQyxlQUFlLEdBQUcsS0FBSyxDQUFBO1FBQzlCLENBQUM7OztPQUpBO0lBTUQsc0JBQVcseUJBQUs7YUFBaEI7WUFDRSxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQTtRQUNwQixDQUFDO2FBRUQsVUFBaUIsS0FBa0I7WUFDakMsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUE7UUFDckIsQ0FBQzs7O09BSkE7SUFNRCxzQkFBVywwQkFBTTthQUFqQjtZQUNFLE1BQU0sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFBO1FBQ3JCLENBQUM7OztPQUFBO0lBRUQsc0JBQVcsMEJBQU07YUFBakI7WUFDRSxNQUFNLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQTtRQUNyQixDQUFDOzs7T0FBQTtJQUVELHNCQUFXLGtDQUFjO2FBQXpCO1lBQ0UsTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLEtBQUssV0FBVyxDQUFDLE9BQU8sQ0FBQTtRQUM1QyxDQUFDOzs7T0FBQTtJQUNILGFBQUM7QUFBRCxDQUFDLEFBbERELElBa0RDO0FBbERZLHdCQUFNO0FBb0RuQixJQUFZLFdBSVg7QUFKRCxXQUFZLFdBQVc7SUFDckIsbURBQU8sQ0FBQTtJQUNQLG1EQUFPLENBQUE7SUFDUCx1REFBUyxDQUFBO0FBQ1gsQ0FBQyxFQUpXLFdBQVcsR0FBWCxtQkFBVyxLQUFYLG1CQUFXLFFBSXRCIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGNsYXNzIFNxdWFyZSB7XG4gIHByaXZhdGUgX3N0YXRlOiBTcXVhcmVTdGF0ZVxuICBwcml2YXRlIF9pc01pbmU6IGJvb2xlYW5cbiAgcHJpdmF0ZSBfeENvb3JkOiBudW1iZXJcbiAgcHJpdmF0ZSBfeUNvb3JkOiBudW1iZXJcbiAgcHJpdmF0ZSBfYWRqU3F1YXJlQ291bnQ6IG51bWJlclxuXG4gIGNvbnN0cnVjdG9yKHg6IG51bWJlciwgeTogbnVtYmVyKSB7XG4gICAgdGhpcy5fc3RhdGUgPSBTcXVhcmVTdGF0ZS5Db3ZlcmVkXG4gICAgdGhpcy5faXNNaW5lID0gZmFsc2VcbiAgICB0aGlzLl94Q29vcmQgPSB4XG4gICAgdGhpcy5feUNvb3JkID0geVxuICAgIHRoaXMuX2FkalNxdWFyZUNvdW50ID0gMFxuICB9XG5cbiAgcHVibGljIGdldCBpc01pbmUoKTogYm9vbGVhbiB7XG4gICAgcmV0dXJuIHRoaXMuX2lzTWluZVxuICB9XG5cbiAgcHVibGljIHNldCBpc01pbmUodmFsdWU6IGJvb2xlYW4pIHtcbiAgICB0aGlzLl9pc01pbmUgPSB2YWx1ZVxuICB9XG5cbiAgcHVibGljIGdldCBhZGpTcXVhcmVDb3VudCgpOiBudW1iZXIge1xuICAgIHJldHVybiB0aGlzLl9hZGpTcXVhcmVDb3VudFxuICB9XG5cbiAgcHVibGljIHNldCBhZGpTcXVhcmVDb3VudCh2YWx1ZTogbnVtYmVyKSB7XG4gICAgdGhpcy5fYWRqU3F1YXJlQ291bnQgPSB2YWx1ZVxuICB9XG5cbiAgcHVibGljIGdldCBzdGF0ZSgpOiBTcXVhcmVTdGF0ZSB7XG4gICAgcmV0dXJuIHRoaXMuX3N0YXRlXG4gIH1cblxuICBwdWJsaWMgc2V0IHN0YXRlKHZhbHVlOiBTcXVhcmVTdGF0ZSkge1xuICAgIHRoaXMuX3N0YXRlID0gdmFsdWVcbiAgfVxuXG4gIHB1YmxpYyBnZXQgeENvb3JkKCk6IG51bWJlciB7XG4gICAgcmV0dXJuIHRoaXMuX3hDb29yZFxuICB9XG5cbiAgcHVibGljIGdldCB5Q29vcmQoKTogbnVtYmVyIHtcbiAgICByZXR1cm4gdGhpcy5feUNvb3JkXG4gIH1cblxuICBwdWJsaWMgZ2V0IGNhbkNoYW5nZVN0YXRlKCk6IGJvb2xlYW4ge1xuICAgIHJldHVybiB0aGlzLl9zdGF0ZSA9PT0gU3F1YXJlU3RhdGUuQ292ZXJlZFxuICB9XG59XG5cbmV4cG9ydCBlbnVtIFNxdWFyZVN0YXRlIHtcbiAgQ292ZXJlZCxcbiAgRmxhZ2dlZCxcbiAgVW5jb3ZlcmVkXG59XG4iXX0=